<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Form\LieuType;
use App\Repository\LieuRepository;
use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/lieu")
 */
class LieuController extends AbstractController
{
    /**
     * @Route("/", name="lieu_index", methods={"GET"})
     */
    public function index(LieuRepository $lieuRepository): Response
    {
        return $this->render('lieu/index.html.twig', [
            'lieus' => $lieuRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="lieu_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $alert = ['alert' => false, 'value' => ''];
        $lieu = new Lieu();
        $form = $this->createForm(LieuType::class, $lieu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $checkSiLieuExiste = $entityManager->getRepository('App:Lieu')->findOneBy(['nom' => $lieu->getNom(), 'rue' => $lieu->getRue(), 'codePostale' => $lieu->getCodePostale()]);
            if (empty($checkSiLieuExiste)) {
                $lieu->setDateDeCreation(new \DateTime('now'));
                $lieu->addNotePar($this->getUser());
                $lieu->setNote(1);
                $entityManager->persist($lieu);
                $entityManager->flush();
                $alert = ['alert' => true, 'value' => "Point de dépistage ajouté : " . $lieu];
            } else {
                //Si lutilisateur connecté à déjà saisi cette adresse la note de change pas
                if (!$checkSiLieuExiste->getNotePar()->contains($this->getUser())) {

                    $checkSiLieuExiste->addNotePar($this->getUser());
                    $checkSiLieuExiste->setNote($checkSiLieuExiste->getNote() + 1);
                    $entityManager->persist($checkSiLieuExiste);
                    $entityManager->flush();
                    $alert = ['alert' => true, 'value' => "Point de dépistage déjà existant, note augmenté à : ".$checkSiLieuExiste->getNote()];
                } else {
                    $alert = ['alert' => true, 'value' => 'Vous ne pouvez pas saisir 2 fois le même point de dépistage'];
                }
            }
            return $this->redirectToRoute('home_alert',['alert'=> implode('-',$alert)]);
        }
        return $this->render('lieu/newLieuModal.html.twig', [
            'lieu' => $lieu,
            'newform' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="lieu_show", methods={"GET"})
     */
    public function show(Lieu $lieu): Response
    {
        return $this->render('lieu/show.html.twig', [
            'lieu' => $lieu,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="lieu_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Lieu $lieu): Response
    {
        $form = $this->createForm(LieuType::class, $lieu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lieu_index');
        }

        return $this->render('lieu/edit.html.twig', [
            'lieu' => $lieu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lieu_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lieu $lieu): Response
    {
        if ($this->isCsrfTokenValid('delete' . $lieu->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lieu);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lieu_index');
    }

    /**
     * @Route("/list/json", name="lieu_liste_json", methods={"GET"})
     */
    public function lieuListJson(Request $request)
    {
        $emRepoLieu = $this->getDoctrine()->getManager()->getRepository('App:Lieu');
        $Lieu = $emRepoLieu->findAll();
        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getNumeroDeRue() . $object->getRue() . $object->getCodePostale();
            },
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $serializer = new Serializer([$normalizer], [$encoder]);
        $ListeLieu = [];
        foreach ($Lieu as $item) {
            $jsonContent = $serializer->serialize($item, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['dateDeCreation', 'notePar']]);
            array_push($ListeLieu, $jsonContent);
        }



        if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {
            $jsonData = array(
                "code" => 200,
                "response" => $ListeLieu);
            return new JsonResponse($jsonData);
        } else {
            $jsonData = array(
                "code" => 400,
                "response" => 'impossible de récuperer les lieux');
            return new JsonResponse($jsonData);
        }
    }

    /**
     * @Route("/list/csvToJson", name="lieu_liste_csv_json", methods={"GET"})
     */
    public function csvAdresseToJson(Request $request){

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(),new JsonEncoder()]);
        $finder = new Finder();
        $finder->in(__DIR__.'/../data');
        $finder->files()->name('*.csv');
        $dataJson= '';
        foreach ($finder as $file) {
            $contents = $file->getContents();
            $datas = $serializer->decode($contents,
                'csv', array(CsvEncoder::DELIMITER_KEY => ','));
            $dataJson = $serializer->encode($datas,'json');
        }
        if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {
            $jsonData = array(
                "code" => 200,
                "response" => $dataJson);
            return new JsonResponse($jsonData);
        } else {
            $jsonData = array(
                "code" => 400,
                "response" => 'impossible de récuperer les lieux');
            return new JsonResponse($jsonData);
        }

    }
}
