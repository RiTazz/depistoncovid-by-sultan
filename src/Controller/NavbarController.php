<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavbarController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(): Response
    {
        $loged = false;
        if($this->getUser() != null)
        {
            $loged = true;
        }
        return $this->render('navbar/index.html.twig', [
            'controller_name' => 'NavbarController', 'loged' => $loged,
            'alert' => '',
        ]);
    }
    /**
     * @Route("/home/{alert}", name="home_alert")
     */
    public function indexx(string $alert): Response
    {
        $alert = explode("-",$alert);
        $loged = false;
        if($this->getUser() != null)
        {
            $loged = true;
        }
        return $this->render('navbar/index.html.twig', [
            'controller_name' => 'NavbarController', 'loged' => $loged,
            'alert' => ['alert'=>$alert[0], 'value' => $alert[1]],
        ]);
    }
}
