<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\RegisterType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/utilisateur", name="utilisateur_")
 */
class UtilisateurController extends AbstractController
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
    }
    
    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request, LoggerInterface $logger): Response
    {
        $utilisateur = new Utilisateur();
        $registerForm = $this->createForm(RegisterType::class, $utilisateur);

        $registerForm->handleRequest($request);
        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $utilisateur = $registerForm->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($utilisateur);
            $entityManager->flush();

        }

        return $this->render('utilisateur/registerModal.html.twig',
                                array('registerForm' => $registerForm->createView()));
    }
}