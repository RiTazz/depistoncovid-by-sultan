<?php

namespace App\Form;

use App\Entity\Lieu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,[
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
            ->add('numeroDeRue',NumberType::class,[
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
            ->add('rue',TextType::class,[
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
            ->add('codePostale',NumberType::class,[
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
            ->add('ville',TextType::class,[
                'attr' =>[
                    'class' => 'form-control'
                ]
            ])
            ->add('Valider', SubmitType::class,[
                'attr' =>[
                    'class' => 'btn btn-primary',
                    'style' => "background-color:#648DE5;"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lieu::class,
        ]);
    }
}
